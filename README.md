Require Python2.7 and pip!
To install: sudo pip install pyopengl pygame
To run: python <exname>.py

To install python:
    Linux:
        sudo dnf install python2
        sudo easy_install pip
    Mac: 
        brew install python
        sudo easy_install pip
